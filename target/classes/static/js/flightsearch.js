$(document).ready(function () {
    $("#flightsTable").hide();
    $("#searchform").submit(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        fire_ajax_submit();

    });

});

function makeTable(data) {
    var rows = '';

    $.each(data, function (rowIndex, r) {
        var row = "<tr>";
        row += "<td>" + r.name + "</td>";
        row += "<td>" + r.source + "</td>";
        row += "<td>" + r.destination + "</td>";
        row += "<td>" + r.departureDate + "</td>";
       // row += "<td>" + r.flightclass + "</td>";
        row += "<td>" + r.totalFare + "</td>";
        row += "</tr>";
        rows += row;
    });
    return rows;
}
function fire_ajax_submit() {

    var search = {}
    search["Source"] = $("#txtFrom").val();
    search["Destination"] = $("#txtTo").val();
    search["Seats"] = $("#txtPassengers").val();
    search["DepartureDate"]=$("#dateDeparture").val();
    search["FlightClass"] = $("#Flightclass").val();
//$('#feedback').html('#Flightclass:selected').text());
//search["Class"] = $('#Flightclass:selected').val();
//    $('#Flightclass').change(function(e) {
//    alert($('#Flightclass').val($(this).val());
//         search["Class"]= $('#Flightclass').val($(this).val());
//
//         txtFrom.val=$('#Flightclass').val($(this).val());
//    });


   // $("#btnSubmit").prop("disabled", true);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/GetFlightsByFilter",
        data: JSON.stringify(search),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            console.log("SUCCESS : ", data);
            $("#flightsTable").show();
            if(data!=null && data.length>0)
            {
                var rows = makeTable(data);
                $('#flightRows').html(rows);
            }
            else
            {
                $("#flightRows").text('No flights found.');
            }
            var json = "<h4>Ajax Response</h4><pre>"
                + JSON.stringify(data, null, 4) + "</pre>";

       // $('#feedback').html(json);

            // console.log("SUCCESS : ", data);
             $("#btnSubmit").prop("disabled", false);

        },
        error: function (e) {

            var json = "<h4>Ajax Response</h4><pre>"
                + e.responseText + "</pre>";
            $('#feedback').html(json);

            console.log("ERROR : ", e);
            $("#btnSubmit").prop("disabled", false);

        }
    });

}

