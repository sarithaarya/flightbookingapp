package org.filghtbooking.ViewModel;

import java.util.Date;

public class SearchCriteria {
    public String Source;
    public String Destination;
    public int Seats;
    public Date DepartureDate;
    public String FlightClass;

    @Override
    public String toString()
    {
        return Source + "----"+Destination+"----"+Seats+"-----"+DepartureDate+"----"+FlightClass;
    }
}
