package org.filghtbooking.Utils;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

public class Helper {
    public static Date getZeroTimeDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        date = calendar.getTime();
        return date;
    }

    public static Date getDateWithoutTimeUsingCalendar(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public static Date NextDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 1); //minus number would decrement the days
        return calendar.getTime();

    }

    public static int GetNoOfWeeksBetweenTwoDates(Date departureDate, Date bookingDate) {
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.set(departureDate.getYear(), departureDate.getMonth(),departureDate.getDay());
        calendar2.set(bookingDate.getYear(), bookingDate.getMonth(),bookingDate.getDay());
        long milliseconds1 = calendar1.getTimeInMillis();
        long milliseconds2 = calendar2.getTimeInMillis();
        long diff = milliseconds2 - milliseconds1;
        int diffWeeks = (int)diff / (7*24 * 60 * 60 * 1000);



        return diffWeeks;
    }

    public static float GetNoOfDaysLeft(Date departureDate) {
        Date date = Helper.getDateWithoutTimeUsingCalendar(new Date());
        long difference = departureDate.getTime() - date.getTime();
        float daysBetween = (difference / (1000*60*60*24));
        return daysBetween;
    }

    public static String GetDayWhenDateGiven(Date date) {
        return new SimpleDateFormat("EE").format(date);
    }
}


