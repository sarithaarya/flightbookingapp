package org.filghtbooking;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;
@SpringBootApplication
public class FlightMain {


    public static void main(String[] args)
    {
        SpringApplication.run(FlightMain.class, args);
    }
}
