package org.filghtbooking.Repository;

//import org.filghtbooking.Model.FlightDetails;
import org.filghtbooking.Utils.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

import org.springframework.jdbc.core.JdbcTemplate;


@Repository
public class FlightRepository {

//@Autowired
//JdbcTemplate jdbcTemplate = new JdbcTemplate();
//    private static List<Flight> flights = new ArrayList<Flight>(
//            addFlights()
//    );

    /*public List<Flight> GetAll() {
        String sql = "select * from flights";


        List<Map<String, Object>> maps= jdbcTemplate.queryForList(sql);
        for(Map<String , Object> entry : maps)
        {
            System.out.println(entry.get("flightID"));
        }


        return flights;
    }

    public Flight Get(String name) {
        Optional<Flight> flight = flights.stream().filter(f -> f.getFlightName().equalsIgnoreCase(name)).findFirst();
        return flight.orElse(null);
    }

    public void Add(Flight flight) {
        flights.add(flight);
    }

    public void Update(Flight flight) {
        Optional<Flight> result = flights.stream().filter(f -> f.getFlightName().equalsIgnoreCase(flight.getFlightName())).findFirst();

        if (result.isPresent()) {
            Flight existingFlight = result.get();
            //existingFlight.setFlightBasePrice(flight.getFlightBasePrice());
            //TODO:: set others
        }
    }

    public void Delete(String name) {
        Optional<Flight> result = flights.stream().filter(f -> f.getFlightName().equalsIgnoreCase(name)).findFirst();
        if (result.isPresent()) {
            flights.remove(result.get());
        }
    }

    private static List<Flight> addFlights() {
        FlghtDetailsList flghtDetailsList = new FlghtDetailsList().invoke();
        List<FlightDetails> BoeingDetails = flghtDetailsList.getBoeingDetails();
        List<FlightDetails> AirBusDetails = flghtDetailsList.getAirBusDetails();
        List<FlightDetails> EmiratesDetails = flghtDetailsList.getEmiratesDetails();

        List<Flight> ltflights = new ArrayList<>();

        Flight fl1 = new Flight("Boeing",BoeingDetails,"HYD","BLR",1);
        Flight fl2 = new Flight("AirBus",AirBusDetails, "HYD","BLR" , 2);
        Flight fl3 = new Flight("Emirates", EmiratesDetails, "PUN", "HYD", 3);
        ltflights.add(fl1);
        ltflights.add(fl2);
        ltflights.add(fl3);
        return ltflights;
    }

    public static class FlghtDetailsList {
        private List<FlightDetails> boeingDetails;
        private List<FlightDetails> airBusDetails;
        private List<FlightDetails> emiratesDetails;

        public List<FlightDetails> getBoeingDetails() {
            return boeingDetails;
        }

        public List<FlightDetails> getAirBusDetails() {
            return airBusDetails;
        }

        public List<FlightDetails> getEmiratesDetails() {
            return emiratesDetails;
        }

        public FlghtDetailsList invoke() {
            boeingDetails = new ArrayList<>();
            boeingDetails.add(new FlightDetails(1,"FC",8, Helper.getZeroTimeDate(new Date()),20000));
            boeingDetails.add(new FlightDetails(1,"BC",34,Helper.getZeroTimeDate(new Date()),13000));
            boeingDetails.add(new FlightDetails(1,"EC",195,Helper.getZeroTimeDate(new Date()),6000));

            airBusDetails = new ArrayList<>();

            airBusDetails.add(new FlightDetails(2,"EC",144,Helper.getZeroTimeDate(new Date()),4000));

            emiratesDetails = new ArrayList<>();
            emiratesDetails.add(new FlightDetails(3,"BC",20,Helper.getZeroTimeDate(new Date()),10000));
            emiratesDetails.add(new FlightDetails(3,"EC",152,Helper.getZeroTimeDate(new Date()),5000));
            return this;
        }
    }*/
}