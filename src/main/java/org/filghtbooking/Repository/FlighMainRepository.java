package org.filghtbooking.Repository;

import org.filghtbooking.Model.*;
import org.filghtbooking.Utils.Helper;
import org.springframework.stereotype.Repository;


import java.util.*;
import java.util.stream.Collectors;

@Repository
public class FlighMainRepository {

    private static List<FlightMain> flights = new ArrayList<FlightMain>(
            addFlights()
    );

    public List<FlightMain> GetAll()
    {
        // Avoid changes to flight by cloning it.
        return addFlights();
    }

    private static List<FlightMain> addFlights() {
        List<FlightClass> flightClasses1 = Arrays.asList(
                new FlightClass("EC", 195, 6000),
                new FlightClass("FC", 8, 20000),
                new FlightClass("BC", 35,13000)
        );

        List<FlightClass> flightClasses2 = Arrays.asList(
                new FlightClass("EC", 144, 4000),
                new FlightClass("FC", 0, 0),
                new FlightClass("BC", 0, 0)
        );

        List<FlightClass> flightClasses3 = Arrays.asList(
                new FlightClass("EC", 152, 5000),
                new FlightClass("FC", 0, 0),
                new FlightClass("BC", 20, 10000)
        );

        List<FlightBooking> flightBookings1 = Arrays.asList(
                new FlightBooking("EC", 100),
                new FlightBooking("FC", 0),
                new FlightBooking("BC", 0)
        );

        List<FlightBooking> flightBookings2 = Arrays.asList(
                new FlightBooking("EC", 0),
                new FlightBooking("FC", 0),
                new FlightBooking("BC", 0)
        );

        List<FlightBooking> flightBookings3 = Arrays.asList(
                new FlightBooking("EC", 0),
                new FlightBooking("FC", 0),
                new FlightBooking("BC", 0)
        );

        Date date = Helper.getDateWithoutTimeUsingCalendar(new Date());
        ArrayList<FlightSchedule> flightSchedules = new ArrayList<FlightSchedule>();
        FlightSchedule fs1 = new FlightSchedule(1, "Hyderabad", "Delhi", date, flightBookings1);
        flightSchedules.add(fs1);
        FlightSchedule fs2 = new FlightSchedule(1, "Delhi", "Hyderabad", date, flightBookings2);
        flightSchedules.add(fs2);
        FlightSchedule fs3 = new FlightSchedule(2, "Banglore", "Hyderabad", date, flightBookings3);
        flightSchedules.add(fs3);
        FlightSchedule fs4 = new FlightSchedule(2, "Hyderabad", "Banglore", date, flightBookings3);
        flightSchedules.add(fs4);
        FlightSchedule fs5 = new FlightSchedule(2, "Hyderabad", "Delhi", date, flightBookings1);
        flightSchedules.add(fs5);
        FlightSchedule fs6 = new FlightSchedule(3, "Banglore", "Hyderabad", date, flightBookings3);
        flightSchedules.add(fs6);

        List<FlightMain> ltflights = new ArrayList<>();

        FlightMain fl1 = new FlightMain(1,"Boeing777_F01","Boeing777",flightClasses1, GetFlightSchedulesForFlight(flightSchedules,1));
        FlightMain fl2 = new FlightMain(2,"AirBus319_F02","AirBus319",flightClasses2,GetFlightSchedulesForFlight(flightSchedules,2));
        FlightMain fl3 = new FlightMain(3,"AirBus321_F03","AirBus321",flightClasses3,GetFlightSchedulesForFlight(flightSchedules,3));
        ltflights.add(fl1);
        ltflights.add(fl2);
        ltflights.add(fl3);
        return ltflights;
    }

    public static List<FlightSchedule> GetFlightSchedulesForFlight(ArrayList<FlightSchedule> flightSchedules, int flightId) {
        return flightSchedules.stream().filter(f->f.getFlightID()==flightId).collect(Collectors.toList());
    }

    public static Optional<FlightMain> GetFlightById(int flightId) {
        return flights.stream().filter(f->f.getFlightID()==flightId).findFirst();
    }



}
