package org.filghtbooking.Services;

import org.filghtbooking.Model.*;
import org.filghtbooking.Repository.FlighMainRepository;
import org.filghtbooking.Utils.Helper;
import org.filghtbooking.ViewModel.SearchCriteria;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import lombok.Data;


@Service
public class FlightSearchService {

    private FlighMainRepository repository;
    private BookingAmountCalculatorService bookingAmountCalculatorService;

    public FlightSearchService(FlighMainRepository repository, BookingAmountCalculatorService bookingAmountCalculatorService) {
        this.repository = repository;
        this.bookingAmountCalculatorService = bookingAmountCalculatorService;
    }

    public List<FlightMain> GetAllFlights() {
        return repository.GetAll();
    }

    public List<FlightDTO> SearchFlights(SearchCriteria searchCriteria) {
        System.out.println(searchCriteria);

        List<FlightMain> flights = GetAllFlights();
        FixSearchCriteria(searchCriteria);
        List<FlightMain> filteredFlights = flights.stream()
                .filter(HavingFlightSchedules())
                .map(FilterFlightSchedulesBySearchCriteria(searchCriteria))
                .filter(FilterFlightsThatHasFlightSchedules())
                .filter(FilterByFlightClassAndSeats(searchCriteria))
                .map(CalculateBookingPrice(searchCriteria))
                .collect(Collectors.toList());

        System.out.println(" ---------- "+searchCriteria);

         List<FlightDTO> dtos = filteredFlights.stream()
                 .flatMap(flight -> flight.getFlightSchedules().stream())
                 .map(schedule -> convertToFlightDTO(schedule, searchCriteria))
                 .collect(Collectors.toList());

         return dtos;
    }

   private FlightDTO convertToFlightDTO(FlightSchedule schedule, SearchCriteria searchCriteria) {
       System.out.println(schedule);
        System.out.println("Called........");
       System.out.println(searchCriteria);
        FlightMain flight = repository.GetFlightById(schedule.getFlightID()).orElse(null);

        FlightDTO dto =new FlightDTO();
                dto.setId(schedule.getFlightID());
                dto.setName(flight.getFlightName());
                dto.setSource(schedule.getSource());
                dto.setDestination(schedule.getDestination());
                dto.setDepartureDate(schedule.getDepartureDate());
                dto.setTotalFare(schedule.getTotalBookingPrice());

//            return FlightDTO.builder()
//                .id(flight.getFlightID())
//                .name(flight.getFlightName())
//                .source(searchCriteria.Source)
//                .destination(searchCriteria.Destination)
//                .departureDate(searchCriteria.DepartureDate)//.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)))
//                .flightClass(searchCriteria.FlightClass)
//                .totalFare(CalculateBookingPrice(searchCriteria))
//                .build();

       System.out.println(dto);
       return dto;
    }

    private Predicate<FlightMain> FilterByFlightClassAndSeats(SearchCriteria searchCriteria) {
        return flight -> {
            FlightClass flightClass = flight.getFlightClassAndSeats().get(searchCriteria.FlightClass);

            FlightSchedule fs = flight.getFlightSchedules().get(0);
            FlightBooking booking = fs.getFlightBookings().stream()
                    .filter(fb -> fb.getFlightClass().equalsIgnoreCase(searchCriteria.FlightClass)).findFirst().get();

            return (flightClass.getTotalSeats() - booking.getReservedSeats()) > searchCriteria.Seats;
        };
    }

    private Predicate<FlightMain> FilterFlightsThatHasFlightSchedules() {
        return f ->
                f.getFlightSchedules().size() > 0;
    }

    private Function<FlightMain, FlightMain> FilterFlightSchedulesBySearchCriteria(SearchCriteria searchCriteria) {
        return flight -> {
            List<FlightSchedule>  flightScheduleList= FilterFlightSchedules(flight.getFlightSchedules(), searchCriteria);
            flight.setFlightSchedules(flightScheduleList);
            return flight;
        };
    }

    private Predicate<FlightMain> HavingFlightSchedules() {
        return f -> f.getFlightSchedules() != null;
}

    private List<FlightSchedule> FilterFlightSchedules(List<FlightSchedule> flightSchedules, SearchCriteria searchCriteria) {
        List<FlightSchedule> flightScheduleList = flightSchedules.stream()
                .filter(fs -> fs.getSource().equalsIgnoreCase(searchCriteria.Source))
                .filter(fs -> fs.getDestination().equalsIgnoreCase(searchCriteria.Destination))
                .filter(fs -> fs.getDepartureDate().compareTo(searchCriteria.DepartureDate) == -1)
                //.map(fs -> convertToFlightDTO(fs,searchCriteria))
                .collect(Collectors.toList());
        return flightScheduleList;
    }

    private void FixSearchCriteria(SearchCriteria searchCriteria) {

        // Fix departure date
        searchCriteria.DepartureDate = (searchCriteria.DepartureDate == null) ? Helper.NextDay(new Date()) : searchCriteria.DepartureDate;
        searchCriteria.DepartureDate = Helper.getDateWithoutTimeUsingCalendar(searchCriteria.DepartureDate);

        // Fix flight class
        if (searchCriteria.FlightClass == null)
            searchCriteria.FlightClass = "EC";

        // Fix number of seats
        if (searchCriteria.Seats == 0) {
            searchCriteria.Seats = 1;
        }
    }

    private Function<FlightMain, FlightMain> CalculateBookingPrice(SearchCriteria searchCriteria) {
        //By this stage, we should have only one flight schedule that matches search criteria

        return flight-> {
            bookingAmountCalculatorService.Calculate(flight, searchCriteria);
            return flight;
        };
    }
}
