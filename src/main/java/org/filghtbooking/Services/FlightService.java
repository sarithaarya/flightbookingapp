package org.filghtbooking.Services;

//import org.filghtbooking.Model.FlightDetails;
import org.filghtbooking.Repository.FlightRepository;
import org.filghtbooking.Utils.Helper;
import org.filghtbooking.ViewModel.SearchCriteria;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FlightService {

    private FlightRepository repository;

    public FlightService(FlightRepository repository) {
        this.repository = repository;
    }

    /*public List<Flight> GetAllFlights() {
        return repository.GetAll();
    }

    public Flight GetFlight(String name) {
        return repository.Get(name);
    }

    public void AddFlight(Flight flight) {
        repository.Add(flight);
    }

    public void UpdateFlight(Flight flight) {
        repository.Update(flight);
    }
*/
    /*public List<Flight> SearchFlights(String source, String destination, int seats) {

        List<Flight> flights = repository.GetAll();
        List<Flight> filteredFlights = new ArrayList<>();

        for(Flight f:flights) {
            if (f.getFlightSource().equalsIgnoreCase(source) && f.getFlightDestination().equalsIgnoreCase(destination) )
            {
                for(FlightDetails fd:f.getFlight_details())
                {
                    if(fd.getNoOfSeats()>seats) {
                        filteredFlights.add(f);
                    }
                    // break;
                }
                 *//* //  f.getFlight_details().stream().filter(fd -> fd.getNoOfSeats() > i).count()>0)
                if (f.getFlight_details().stream().filter(fd -> fd.getNoOfSeats() > i).count() > 0)
                    FilteredFlights.add(f);*//*
            }
        }
        return filteredFlights;
    }*/

    /*public List<Flight> SearchFlights(SearchCriteria searchCriteria)
    {
       *//* List<Flight> flights = repository.GetAll();
        FixSearchCriteria(searchCriteria);

        List<Flight> results = flights.stream()
                .filter(f->  f.getFlightSource().equalsIgnoreCase(searchCriteria.Source))
                .filter(f-> f.getFlightDestination().equalsIgnoreCase(searchCriteria.Destination))
                .filter(f-> f.getFlight_details()!=null)
                .map(f-> {
                    f.setFlight_details(FilterFlightDetails(f.getFlight_details(), searchCriteria));
                    return f;
                })
                .filter(f-> f.getFlight_details().size()>0)
                .collect(Collectors.toList());


//        List<Flight> results = flights.stream()
//                .filter(f->  f.getFlightSource().equalsIgnoreCase(searchCriteria.Source))
//                .filter(f-> f.getFlightDestination().equalsIgnoreCase(searchCriteria.Destination))
//                .filter(f-> f.getFlight_details()!=null)
//                .filter(f-> f.getFlight_details().stream()
//                        .filter(fd-> fd.getFlightClass() == searchCriteria.Class)
//                        .anyMatch(fd-> fd.getNoOfSeats()>=searchCriteria.Seats &&
//                                fd.getDepartureDate() .compareTo(searchCriteria.DepartureDate) == -1))
//                .collect(Collectors.toList());
        return results;
*//*
    return null;
    }*/


    /* private List<FlightDetails> FilterFlightDetails(List<FlightDetails> flightDetails, SearchCriteria searchCriteria) {
        return flightDetails.stream()
                .filter(fd-> fd.getFlightClass() == searchCriteria.FlightClass)
                .filter(fd-> fd.getNoOfSeats()>=searchCriteria.Seats &&
                        fd.getDepartureDate() .compareTo(searchCriteria.DepartureDate) == -1)
                .collect(Collectors.toList());
    }*/
    private void FixSearchCriteria(SearchCriteria searchCriteria) {
        if(searchCriteria.Seats ==0){
            searchCriteria.Seats =1;
        }

        LocalDate now = LocalDate.now().plusDays(1);
        if(searchCriteria.DepartureDate == null) {
            searchCriteria.DepartureDate = Helper.getZeroTimeDate( Date.from(now.atStartOfDay(ZoneId.systemDefault()).toInstant()));
        }
        else{
            searchCriteria.DepartureDate = Helper.getZeroTimeDate(searchCriteria.DepartureDate);
        }
        if(searchCriteria.FlightClass == null)
            searchCriteria.FlightClass ="EC";
    }



}