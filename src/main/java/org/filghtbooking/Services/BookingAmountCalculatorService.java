package org.filghtbooking.Services;

import org.filghtbooking.Model.FlightBooking;
import org.filghtbooking.Model.FlightClass;
import org.filghtbooking.Model.FlightMain;
import org.filghtbooking.Model.FlightSchedule;
import org.filghtbooking.Utils.Helper;
import org.filghtbooking.ViewModel.SearchCriteria;
import org.springframework.stereotype.Service;

import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.function.Predicate;

@Service
public class BookingAmountCalculatorService {

    Date date = Helper.getDateWithoutTimeUsingCalendar(new Date());

    public void Calculate(FlightMain flight, SearchCriteria searchCriteria) {

        if (flight == null || flight.getFlightSchedules().size() <= 0) {
            return;
        }

        switch (searchCriteria.FlightClass){
            case "EC":
                CalculateBookingFareForEconomyClass(flight, searchCriteria);
                break;
            case "FC":
                CalculateBookingFareForFirstClass(flight, searchCriteria);
                break;
            case "BC":
                CalculateBookingFareForBusinessClass(flight, searchCriteria);
                break;
            default:
                throw new InvalidParameterException("FlightClass");
        }
    }

    private void CalculateBookingFareForEconomyClass(FlightMain flight, SearchCriteria searchCriteria) {
        FlightSchedule fs = flight.getFlightSchedules().get(0);

        FlightClass fc = flight.getFlightClassAndSeats().get(searchCriteria.FlightClass);

        int avaialbleSeats = GetAvailableSeatsByClass(flight, searchCriteria);

        if(avaialbleSeats  <= GetSeatsByPercentage(fc, 10)) {
            fs.setTotalBookingPrice( searchCriteria.Seats * (fc.getBaseFare() + (fc.getBaseFare()*0.6)));
        }
        else if(avaialbleSeats <= GetSeatsByPercentage(fc, 50)) {
            fs.setTotalBookingPrice( searchCriteria.Seats * (fc.getBaseFare() + (fc.getBaseFare()*0.3)));
        }
        else {
            fs.setTotalBookingPrice( searchCriteria.Seats * fc.getBaseFare());
        }
    }

    private int GetAvailableSeatsByClass(FlightMain flight, SearchCriteria searchCriteria) {
            FlightClass flightClass = flight.getFlightClassAndSeats().get(searchCriteria.FlightClass);

            FlightSchedule fs = flight.getFlightSchedules().get(0);
            FlightBooking booking = fs.getFlightBookings().stream()
                    .filter(fb -> fb.getFlightClass().equalsIgnoreCase(searchCriteria.FlightClass)).findFirst().get();

            return (flightClass.getTotalSeats() - booking.getReservedSeats());
    }

    private int GetSeatsByPercentage(FlightClass fc, int percentage) {
        return (int) ((percentage/100d) *fc.getTotalSeats());
    }

    private void CalculateBookingFareForFirstClass(FlightMain flight, SearchCriteria searchCriteria) {
        FlightSchedule fs = flight.getFlightSchedules().get(0);
        FlightClass fc = flight.getFlightClassAndSeats().get(searchCriteria.FlightClass);
        Date departureDate=searchCriteria.DepartureDate;
        if(Helper.GetNoOfDaysLeft(departureDate) <= 10)
        {
            fs.setTotalBookingPrice( searchCriteria.Seats *
                    (fc.getBaseFare() + (fc.getBaseFare()*((Helper.GetNoOfDaysLeft(departureDate)*10)/100d))));
        }

        fs.setTotalBookingPrice( searchCriteria.Seats * fc.getBaseFare());
    }

    private void CalculateBookingFareForBusinessClass(FlightMain flight, SearchCriteria searchCriteria) {

        if (Helper.GetNoOfWeeksBetweenTwoDates(searchCriteria.DepartureDate, date) <= 4) {
            FlightSchedule fs = flight.getFlightSchedules().get(0);
            FlightClass fc = flight.getFlightClassAndSeats().get(searchCriteria.FlightClass);
            Date date = fs.getDepartureDate();
            String day = Helper.GetDayWhenDateGiven(date);
            if (day == "Mon" || day == "Fri" || day == "Sun")
                fs.setTotalBookingPrice(searchCriteria.Seats * (fc.getBaseFare() + (fc.getBaseFare() * 0.4)));
            else fs.setTotalBookingPrice(searchCriteria.Seats * fc.getBaseFare());
        }
    }


}
