package org.filghtbooking.Controller;

import org.filghtbooking.Model.FlightDTO;
import org.filghtbooking.Model.FlightMain;
import org.filghtbooking.Services.FlightSearchService;
import org.filghtbooking.ViewModel.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

//import java.net.http.HttpResponse;
import java.util.List;

@RestController
//@RequestMapping("/flight")
public class FlightController {

    @Autowired
    FlightSearchService service;

    @RequestMapping("/welcome")
    public String welcome() {
        return "welcome";
    }

    @RequestMapping("/")
    @CrossOrigin
    public List<FlightMain> GetAll() {
        return service.GetAllFlights();
    }
/*
    @RequestMapping(method = RequestMethod.GET,path="/Get")
    public Flight Get(@RequestParam  String name) {
        return service.GetFlight(name);
    }
/*@RequestMapping(method = RequestMethod.GET, path="/GetFlightsByFilter")
    public List<Flight> GetFlightsByFilter(@RequestParam String source, @RequestParam String destination,@RequestParam int noOfSeats)
    {
        return service.GetFlightWithSourceAndDestination(source,destination,noOfSeats);
    }*/
    @PostMapping("/GetFlightsByFilter")
    @CrossOrigin
    public List<FlightDTO> searchResultViaAjax(@RequestBody SearchCriteria searchCriteria, Errors errors) {
       /* public ResponseEntity<?> searchResultViaAjax(@RequestBody SearchCriteria searchCriteria, Errors errors) {
       AjaxResponseBody ajaxresponse = new AjaxResponseBody();
        //If error, just return a 400 bad request, along with the error message
        if (errors.hasErrors()) {
            ajaxresponse.setMsg(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));
            ResponseEntity.badRequest().body(ajaxresponse);
        }
       // ajaxresponse.result = service.SearchFlights(searchFlight.getFlightSource(), searchFlight.getFlightDestination(), searchFlight.getNoOfSeats());
        //SearchCriteria searchCriteria = new SearchCriteria();
        ajaxresponse.result = service.SearchFlights(searchCriteria);
        ajaxresponse.setResult(ajaxresponse.result);
        return ResponseEntity.ok(ajaxresponse);*/

        return service.SearchFlights(searchCriteria);
    }

}
