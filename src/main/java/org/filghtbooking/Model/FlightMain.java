package org.filghtbooking.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FlightMain {
    int flightID;
    String flightName;
    String flightCarrier;

    @JsonIgnore
    private Map<String , FlightClass> flightClassAndSeats = new HashMap<String,FlightClass>();

    List<FlightSchedule> flightSchedules;

    private List<FlightClass> flightClasses;

    public List<FlightSchedule> getFlightSchedules() {
        return flightSchedules;
    }

    public void setFlightSchedules(List<FlightSchedule> flightSchedules) {
        this.flightSchedules = flightSchedules;
    }

    public FlightMain(int flightID, String flightName, String flightCarrier, List<FlightClass> flightClasses,List<FlightSchedule> flightSchedules) {
        this.flightID = flightID;
        this.flightName = flightName;
        this.flightCarrier = flightCarrier;
        this.flightClasses = flightClasses;
        this.flightSchedules = flightSchedules;

        this.flightClassAndSeats = this.flightClasses.stream().collect(Collectors.toMap(fc->fc.getFlightClass(), fc-> fc));
    }

    public FlightMain() {
    }

    public int getFlightID() {
        return flightID;
    }

    public void setFlightID(int flightID) {
        this.flightID = flightID;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public String getFlightCarrier() {
        return flightCarrier;
    }

    public void setFlightCarrier(String flightCarrier) {
        this.flightCarrier = flightCarrier;
    }

    public List<FlightClass> getFlightClasses() {
        return flightClasses;
    }

    public void setFlightClasses(List<FlightClass> flightClasses) {
        this.flightClasses = flightClasses;
    }

    public Map<String, FlightClass> getFlightClassAndSeats() {
        return flightClassAndSeats;
    }

    @Override
    public String toString()
    {
        return flightName;
    }
}
