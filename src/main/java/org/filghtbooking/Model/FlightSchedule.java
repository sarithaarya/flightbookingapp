package org.filghtbooking.Model;

import java.util.Date;
import java.util.List;

public class FlightSchedule {

    int flightID;
    String source;
    String destination;
    Date departureDate;
    double TotalBookingPrice;

    private List<FlightBooking> flightBookings;

    public double getTotalBookingPrice() {
        return TotalBookingPrice;
    }

    public void setTotalBookingPrice(double totalBookingPrice) {
        TotalBookingPrice = totalBookingPrice;
    }

    public FlightSchedule(int flightID, String source, String destination, Date departureDate, List<FlightBooking> flightBookings) {
        this.flightID = flightID;
        this.source = source;
        this.destination = destination;
        this.departureDate = departureDate;
        this.flightBookings = flightBookings;
    }

    public int getFlightID() {
        return flightID;
    }

    public void setFlightID(int flightID) {
        this.flightID = flightID;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public List<FlightBooking> getFlightBookings() {
        return flightBookings;
    }

    public void setFlightBookings(List<FlightBooking> flightBookings) {
        this.flightBookings = flightBookings;
    }

    @Override
    public String toString()
    {
        return " >>>>> "+source + " >>>>> "+ destination + " >>>>> "+departureDate+" >>>>> "+TotalBookingPrice ;
    }

}
