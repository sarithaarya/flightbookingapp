package org.filghtbooking.Model;

import java.util.List;

public class AjaxResponseBody {
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<FlightDTO> getResult() {
        return result;
    }

    public void setResult(List<FlightDTO> result) {
        this.result = result;
    }

   public String msg;
   public List<FlightDTO> result;
}
