package org.filghtbooking.Model;

public class FlightBooking {

    public FlightBooking(String flightClass, int reservedSeats){

        this.flightClass = flightClass;
        this.reservedSeats = reservedSeats;
    }

    private String flightClass;
    private int reservedSeats;

    public String getFlightClass() {
        return flightClass;
    }

    public void setFlightClass(String flightClass) {
        this.flightClass = flightClass;
    }

    public int getReservedSeats() {
        return reservedSeats;
    }

    public void setReservedSeats(int reservedSeats) {
        this.reservedSeats = reservedSeats;
    }
}
