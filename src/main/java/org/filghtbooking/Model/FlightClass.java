package org.filghtbooking.Model;

public class FlightClass {

    public FlightClass(String flightClass, int totalSeats,double baseFare){

        this.flightClass = flightClass;
        this.totalSeats = totalSeats;
        this.baseFare = baseFare;
    }

    private String flightClass;
    private int totalSeats;
    private double baseFare;

    public String getFlightClass() {
        return flightClass;
    }

    public void setFlightClass(String flightClass) {
        this.flightClass = flightClass;
    }

    public int getTotalSeats() {
        return totalSeats;
    }

    public void setTotalSeats(int totalSeats) {
        this.totalSeats = totalSeats;
    }

    public double getBaseFare() {
        return baseFare;
    }

    public void setBaseFare(double baseFare) {
        this.baseFare = baseFare;
    }
}
