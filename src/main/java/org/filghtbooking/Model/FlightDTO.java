package org.filghtbooking.Model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.Data;

import java.util.Date;

//@Getter
//@Setter
//@Builder
public class FlightDTO {

    private int id;
    private String name;
    private String source;
    private String destination;
    private int seats;
    private Date departureDate;
    private String flightClass;
    private double totalFare;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public String getFlightClass() {
        return flightClass;
    }

    public void setFlightClass(String flightClass) {
        this.flightClass = flightClass;
    }

    public double getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(double totalFare) {
        this.totalFare = totalFare;
    }

    @Override
    public String toString()
    {
        return name+" >>>>> "+source + " >>>>> "+ destination + " >>>>> "+seats+" >>>>> "+departureDate+" >>>>> "+flightClass ;
    }

}

//    private FlightDTO convertToFlightDTO(FlightSchedule schedule,FlightSearchCriteria flightSearchCriteria) {
//        Flight flight = flightRepository.getFlightById(schedule.getFlightId()).orElse(null);
//        return FlightDTO.builder()
//                .id(flight.getId())
//                .name(flight.getName())
//                .origin(flightSearchCriteria.getSource())
//                .destination(flightSearchCriteria.getDestination())
//                .departureDate(flightSearchCriteria.getDepartureDateValue().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)))
//                .travelType(flightSearchCriteria.getTravelTypeValue())
//                .totalFare(calculateTotalFare(flight, schedule,flightSearchCriteria))
//                .build();
//    }

