package org.filghtbooking.Services;

import org.filghtbooking.Model.FlightBooking;
import org.filghtbooking.Model.FlightClass;
import org.filghtbooking.Model.FlightMain;
import org.filghtbooking.Model.FlightSchedule;
import org.filghtbooking.Repository.FlighMainRepository;
import org.filghtbooking.Utils.Helper;
import org.filghtbooking.ViewModel.SearchCriteria;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class BookingAmountCalculatorServiceTest {

    @Mock
    FlighMainRepository flighMainRepository;



    @Test
    public void ShouldReturnDayWhenDateGiven() throws ParseException {
        BookingAmountCalculatorService bs = new BookingAmountCalculatorService();
        //Date date = Helper.getDateWithoutTimeUsingCalendar("2019-08-24");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String day = Helper.GetDayWhenDateGiven(sdf.parse("2019-08-24"));
        assertEquals("Sat",day);
    }

    @Test
    public void ShouldCalculateBookingFareForBusinessClass() {

        BookingAmountCalculatorService bs = new BookingAmountCalculatorService();
        Mockito.when(flighMainRepository.GetAll()).thenReturn(getFlights());

        FlightMain flt = getFlights().get(0);
        FlightSchedule fs = flt.getFlightSchedules().get(0);
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.Source= "Hyderabad";
        searchCriteria.Destination = "Delhi";
        searchCriteria.FlightClass ="BC";
        searchCriteria.Seats = 2;
        searchCriteria.DepartureDate= new Date();
        bs.Calculate(flt,searchCriteria);

        assertEquals(26000,fs.getTotalBookingPrice(),0.0);

    }

    @Test
    public void ShouldCalculateBookingFareForFirstClass() {

      BookingAmountCalculatorService bs = new BookingAmountCalculatorService();
        Mockito.when(flighMainRepository.GetAll()).thenReturn(getFlights());

        FlightMain flt = getFlights().get(0);
        FlightSchedule fs = flt.getFlightSchedules().get(0);
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.Source= "Hyderabad";
        searchCriteria.Destination = "Delhi";
        searchCriteria.FlightClass ="FC";
        searchCriteria.Seats = 2;
        searchCriteria.DepartureDate= new Date();
        bs.Calculate(flt,searchCriteria);

        assertEquals(40000,fs.getTotalBookingPrice(),0.0);

    }

    @Test
    public void ShouldCalculateBookingFareForEconomyClass() {

        BookingAmountCalculatorService bs = new BookingAmountCalculatorService();
        Mockito.when(flighMainRepository.GetAll()).thenReturn(getFlights());

        FlightMain flt = getFlights().get(0);
        FlightSchedule fs = flt.getFlightSchedules().get(0);
        List<FlightBooking> fb = fs.getFlightBookings();
        fb.get(0).setReservedSeats(100);
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.Source= "Hyderabad";
        searchCriteria.Destination = "Delhi";
        searchCriteria.FlightClass ="EC";
        searchCriteria.Seats = 2;
        searchCriteria.DepartureDate= new Date();
        bs.Calculate(flt,searchCriteria);

        assertEquals(15600,fs.getTotalBookingPrice(),0.0);

    }

    private static List<FlightMain> getFlights() {

        List<FlightClass> flightClasses1 = Arrays.asList(
                new FlightClass("EC", 195, 6000),
                new FlightClass("FC", 8, 20000),
                new FlightClass("BC", 35,13000)
        );

        List<FlightClass> flightClasses2 = Arrays.asList(
                new FlightClass("EC", 144, 4000),
                new FlightClass("FC", 0, 0),
                new FlightClass("BC", 0, 0)
        );

        List<FlightClass> flightClasses3 = Arrays.asList(
                new FlightClass("EC", 152, 5000),
                new FlightClass("FC", 0, 0),
                new FlightClass("BC", 20, 10000)
        );

        List<FlightBooking> flightBookings1 = Arrays.asList(
                new FlightBooking("EC", 0),
                new FlightBooking("FC", 0),
                new FlightBooking("BC", 0)
        );

        List<FlightBooking> flightBookings2 = Arrays.asList(
                new FlightBooking("EC", 0),
                new FlightBooking("FC", 0),
                new FlightBooking("BC", 0)
        );

        List<FlightBooking> flightBookings3 = Arrays.asList(
                new FlightBooking("EC", 0),
                new FlightBooking("FC", 0),
                new FlightBooking("BC", 0)
        );

        Date date = Helper.getDateWithoutTimeUsingCalendar(new Date());
        ArrayList<FlightSchedule> flightSchedules = new ArrayList<FlightSchedule>();
        FlightSchedule fs1 = new FlightSchedule(1, "Hyderabad", "Delhi", date , flightBookings1);
        flightSchedules.add(fs1);
        FlightSchedule fs2 = new FlightSchedule(1, "Delhi", "Hyderabad", date, flightBookings2);
        flightSchedules.add(fs2);
        FlightSchedule fs3 = new FlightSchedule(2, "Banglore", "Hyderabad", date, flightBookings3);
        flightSchedules.add(fs3);
        FlightSchedule fs4 = new FlightSchedule(2, "Hyderabad", "Banglore", date, flightBookings3);
        flightSchedules.add(fs4);

        List<FlightMain> ltflights = new ArrayList<>();

        FlightMain fl1 = new FlightMain(1,"f01","Boeing777",flightClasses1,flightSchedules);
        FlightMain fl2 = new FlightMain(2,"f02","AirBus319",flightClasses2,flightSchedules);
        FlightMain fl3 = new FlightMain(3,"f03","AirBus321",flightClasses3,flightSchedules);
        ltflights.add(fl1);
        ltflights.add(fl2);
        ltflights.add(fl3);
        return ltflights;
    }

}