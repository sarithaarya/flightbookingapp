package org.filghtbooking.Services;

import org.filghtbooking.Model.*;
import org.filghtbooking.Repository.FlighMainRepository;
import org.filghtbooking.Utils.Helper;
import org.filghtbooking.ViewModel.SearchCriteria;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static junit.framework.TestCase.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class FlightSearchServiceTest {
    @Mock
    public FlighMainRepository repository;

    @Mock
    public BookingAmountCalculatorService calculatorService;

    @Test
    public void ShouldReturnEmptyListWhenNoSource()
    {
        //Arrange
        // FlightRepository repository = Mockito.mock(FlightRepository.class);
        FlightSearchService ftService = new FlightSearchService(repository, calculatorService);
        Mockito.when(repository.GetAll()).thenReturn(getFlights());

        SearchCriteria criteria = new SearchCriteria();
        criteria.Seats = 0;
        criteria.Source = "";
        criteria.Destination ="";

        //Action
//        List<FlightMain> flt = ftService.SearchFlights(criteria);
        List<FlightDTO> flt = ftService.SearchFlights(criteria);
        //Assert
        assertEquals(0,flt.size());
    }

    private static List<FlightMain> getFlights() {

        List<FlightClass> flightClasses1 = Arrays.asList(
                new FlightClass("EC", 195, 6000),
                new FlightClass("FC", 8, 20000),
                new FlightClass("BC", 35,13000)
        );

        List<FlightClass> flightClasses2 = Arrays.asList(
                new FlightClass("EC", 144, 4000),
                new FlightClass("FC", 0, 0),
                new FlightClass("BC", 0, 0)
        );

        List<FlightClass> flightClasses3 = Arrays.asList(
                new FlightClass("EC", 152, 5000),
                new FlightClass("FC", 0, 0),
                new FlightClass("BC", 20, 10000)
        );

        List<FlightBooking> flightBookings1 = Arrays.asList(
                new FlightBooking("EC", 0),
                new FlightBooking("FC", 0),
                new FlightBooking("BC", 0)
        );

        List<FlightBooking> flightBookings2 = Arrays.asList(
                new FlightBooking("EC", 0),
                new FlightBooking("FC", 0),
                new FlightBooking("BC", 0)
        );

        List<FlightBooking> flightBookings3 = Arrays.asList(
                new FlightBooking("EC", 0),
                new FlightBooking("FC", 0),
                new FlightBooking("BC", 0)
        );

        Date date = Helper.getDateWithoutTimeUsingCalendar(new Date());
        ArrayList<FlightSchedule> flightSchedules = new ArrayList<FlightSchedule>();
        FlightSchedule fs1 = new FlightSchedule(1, "Hyderabad", "Delhi", date , flightBookings1);
        flightSchedules.add(fs1);
        FlightSchedule fs2 = new FlightSchedule(1, "Delhi", "Hyderabad", date, flightBookings2);
        flightSchedules.add(fs2);
        FlightSchedule fs3 = new FlightSchedule(2, "Banglore", "Hyderabad", date, flightBookings3);
        flightSchedules.add(fs3);
        FlightSchedule fs4 = new FlightSchedule(2, "Hyderabad", "Banglore", date, flightBookings3);
        flightSchedules.add(fs4);

        List<FlightMain> ltflights = new ArrayList<>();

        FlightMain fl1 = new FlightMain(1,"f01","Boeing777",flightClasses1,flightSchedules);
        FlightMain fl2 = new FlightMain(2,"f02","AirBus319",flightClasses2,flightSchedules);
        FlightMain fl3 = new FlightMain(3,"f03","AirBus321",flightClasses3,flightSchedules);
        ltflights.add(fl1);
        ltflights.add(fl2);
        ltflights.add(fl3);
        return ltflights;
    }
}