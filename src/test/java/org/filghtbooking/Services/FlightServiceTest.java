package org.filghtbooking.Services;

//import org.filghtbooking.Model.FlightDetails;
import org.filghtbooking.Repository.FlightRepository;

import org.filghtbooking.ViewModel.SearchCriteria;
import  org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static junit.framework.TestCase.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public  class FlightServiceTest {

    @Mock
    private  FlightRepository repository;

//    @Test
//    public void ShouldReturnEmptyListWhenNoSource()
//    {
//        //Arrange
//       // FlightRepository repository = Mockito.mock(FlightRepository.class);
//        FlightService ftService = new FlightService(repository);
//        Mockito.when(repository.GetAll()).thenReturn(getFlights());
//
//        SearchCriteria criteria = new SearchCriteria();
//        criteria.Seats = 5;
//        criteria.Source = "HYD";
//        criteria.Destination ="PUN";
//
//        //Action
//        List<Flight> flt = ftService.SearchFlights(criteria);
//
//        //Assert
//        assertEquals(0,flt.size());
//    }

   /* @Test
    public void ShouldReturnEmptyListWhenNoSource()
    {
        //Arrange
         FlightMainRepository repository = Mockito.mock(FlightMainRepository.class);
        FlightService ftService = new FlightService(repository);
        Mockito.when(repository.GetAll()).thenReturn(getFlights());

        SearchCriteria criteria = new SearchCriteria();
        criteria.Seats = 5;
        criteria.Source = "HYD";
        criteria.Destination ="PUN";

        //Action
        List<FlightMain> flt = ftService.SearchFlights(criteria);

        //Assert
        assertEquals(0,flt.size());
    }
    @Test
    public void  ShouldReturnListwithAtleastOneSeatWhenNoSeatsGiven()
    {
        FlightService flightService = new FlightService(repository);
        Mockito.when(repository.GetAll()).thenReturn(getFlights());
        SearchCriteria criteria = new SearchCriteria();
        criteria.Seats = 0;
        criteria.Source = "HYD";
        criteria.Destination ="PUN";
       criteria.DepartureDate= new Date();
       criteria.FlightClass ="";

        List<Flight> flt = flightService.SearchFlights(criteria);

        assertEquals(1,flt.size());
    }

    public void  ShouldReturnListwithEconomyClassWhenNoClassGiven()
    {
        FlightService flightService = new FlightService(repository);
        Mockito.when(repository.GetAll()).thenReturn(getFlights());
        SearchCriteria criteria = new SearchCriteria();
        criteria.Seats = 12;
        criteria.Source = "HYD";
        criteria.Destination ="PUN";
        criteria.DepartureDate= new Date();
        criteria.FlightClass ="";

        List<Flight> flt = flightService.SearchFlights(criteria);

        assertEquals(1,flt.size());
    }

    private static List<Flight> getFlights() {

        List<Flight> flights= new ArrayList<Flight>();

        List<FlightDetails>  boeingDetails = new ArrayList<>();
        boeingDetails.add(new FlightDetails(1,"FC",8,new Date(),20000));

        Flight fl1 = new Flight("Boeing",boeingDetails,"HYD","BLR",1);
        Flight fl2 = new Flight("AirBus",boeingDetails, "HYD","BLR" , 2);
        Flight fl3 = new Flight("Emirates", boeingDetails, "PUN", "HYD", 3);

        flights.add(fl1);
        flights.add(fl2);
        flights.add(fl3);
        return flights;
    }
*/
}